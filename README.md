# MarketMaker

The MarketMaker is a Python-based script utilizing the `pybit` library to perform high-frequency market making on Bitget's trading platform.

## Key Parameters

- **Current mid price (`s`):** The present middle price of the market.
- **Inventory position (`q`):** This signifies the current inventory position.
- **Rate of arrival of market orders (`gamma`):** This is the expected frequency of incoming market orders.
- **Volatility (`sigma`):** The measure of the asset price variation.
- **Time horizon (`T`):** The specified time period for which the calculations are valid.
- **Current time (`t`):** The existing time stamp.
- **Intensity of order arrivals (`k`):** This signifies the strength or rate of incoming order arrivals.
- 
## Sample Calculations
```python
from math import log

# Parameters
sigma = 0.02  # volatility
gamma = 0.1  # rate of order arrivals
T = 8.0  # time horizon
t = 0.0  # current time
k = 1.5  # intensity of order arrivals
eta = 0.1  # transaction cost parameter

# Inventory
current_inventory = 100  # current inventory
target_inventory = 80  # target inventory
q = current_inventory - target_inventory  # inventory imbalance

# Mid-market price
s = 100.0  # let's assume the mid-market price is 100

# Calculate the reservation price
r = s - q * gamma * sigma ** 2 * (T - t)

# Calculate the bid and ask prices
spread = gamma * sigma ** 2 * (T - t) + 2 / gamma * log(1 + gamma / k)
bid = r - spread / 2
ask = r + spread / 2
```