import asyncio
import math
import numpy as np
import time
from pybit.unified_trading import HTTP, WebSocket
from collections import deque
from decimal import Decimal, getcontext
import datetime
import configparser
from scipy.optimize import curve_fit


class MarketMaker:
    def __init__(self):
        config = configparser.ConfigParser()
        config.read('config.ini')
        self.api_key = config.get('API', 'API_KEY')
        self.api_secret = config.get('API', 'API_SECRET')
        self.session = HTTP(
            testnet=False,
            api_key=self.api_key,
            api_secret=self.api_secret,
        )

        self.order_book = {}
        self.start_time = int(time.time())
        self.order_record = []
        self.last_mid_price = None
        self.trades = deque()

        self.target_inventory = Decimal(100)

        self.symbol = 'BTCUSDT'
        self.size = '0.001'
        self.feerate = 0.0002

        getcontext().prec = 8

        self.market_info = {}
        self.price_scale = ''

    def calculate_sigma(self, data):
        prices = np.array([float(d['p']) for d in data])
        returns = np.log(prices[1:]) - np.log(prices[:-1])
        sigma = np.std(returns)
        return sigma

    # TODO
    def calculate_gamma(self, data):
        pass
        # num_orders = len(data)
        # duration = data[-1]['T'] - data[0]['T']
        # gamma = num_orders / duration
        # return gamma

    # TODO
    def caculate_decay(self):
        pass
        # return k

    def caculate_time_interval(self):
        now = datetime.datetime.now()
        horizons = [datetime.time(hour) for hour in (0, 8, 16)]

        # Find the next trading horizon
        T = next((now.replace(hour=horizon.hour, minute=0, second=0, microsecond=0) for horizon in horizons if
                  now.time() < horizon),
                 now.replace(hour=0, minute=0, second=0, microsecond=0) + datetime.timedelta(days=1))

        # Calculate the time remaining until the end of the trading horizon
        return (T - now).total_seconds() / 60  # this mean a minute

    def calculate_bid_ask_from_orderbook(self):
        bids = self.order_book['bids']
        asks = self.order_book['asks']
        bid_price = max(bids.keys())
        ask_price = min(asks.keys())
        return bid_price, ask_price

    def update_order_book(self, data):
        for side in ('b', 'a'):
            book_side = self.order_book['bids'] if side == 'b' else self.order_book['asks']
            for price, amount in data[side]:
                price = float(price)
                amount = float(amount)
                if amount == 0:
                    book_side.pop(price, None)
                else:
                    book_side[price] = amount

    def calculate_bid_ask_price(self, s, sigma, gamma, horizon, k, eta):
        s = Decimal(s)
        sigma = Decimal(sigma)
        gamma = Decimal(gamma)
        horizon = Decimal(horizon)
        k = Decimal(k)

        spread = gamma * sigma ** 2 * horizon + 2 / gamma * np.log(1 + gamma / k)

        r_ask = s + spread / 2
        r_bid = s - spread / 2
        ask = r_ask + r_ask * eta
        bid = r_bid - r_bid * eta
        return Decimal(bid), Decimal(ask)

    async def update_reservation(self):
        current_time = int(time.time())
        if self.start_time and current_time - self.start_time >= 60:
            bid_price, ask_price = self.calculate_bid_ask_from_orderbook()
            s = (bid_price + ask_price) / 2  # Update the mid-price

            # Only update the reservation price if the mid-price has changed
            if s > 0 and self.last_mid_price is None or self.last_mid_price != s:
                self.last_mid_price = s  # Update the last mid price
                sigma = self.calculate_sigma(self.trades)
                gamma = self.calculate_gamma()
                horizon = self.caculate_time_interval()
                k = self.caculate_decay()
                eta = Decimal(self.feerate)

                bid, ask = self.calculate_bid_ask_price(s, sigma, gamma, horizon, k, eta)
                # print(round(ask, int(self.price_scale)), round(bid, int(self.price_scale)))
                await self.async_cancel_all_orders()
                await self.order_tasks(round(ask, int(self.price_scale)), round(bid, int(self.price_scale)))

    def handle_trade(self, trade):
        current_time = int(time.time())
        trade_time = int(trade['T']) // 1000  # convert from ms to s

        # If the trade happened in the last 60 seconds, add it to our list
        if current_time - trade_time <= 60:
            self.trades.append(trade)

        # Remove trades that are more than 60 seconds old
        while self.trades and current_time - int(self.trades[0]['T']) // 1000 > 60:
            self.trades.popleft()

    def handle_message(self, loop, message):
        topic = message.get('topic')
        if topic is None:
            print('No topic in message:', message)
            return

        if topic == 'orderbook.50.{}'.format(self.symbol):
            data = message['data']
            if message['type'] == 'snapshot':
                self.order_book['bids'] = {float(price): float(amount) for price, amount in data['b']}
                self.order_book['asks'] = {float(price): float(amount) for price, amount in data['a']}
            elif message['type'] == 'delta':
                self.update_order_book(data)
            loop.create_task(self.update_reservation())
        elif topic == 'publicTrade.{}'.format(self.symbol):
            trades_data = message['data'][0]
            self.handle_trade(trades_data)
        else:
            print('Unhandled topic:', message)

    def handle_private_order_message(self, message):
        order_data = message['data'][0]

        # Prepare order log entry
        order_id = order_data['orderId']
        symbol = order_data['symbol']
        side = order_data['side']
        price = order_data['price']
        qty = order_data['qty']
        order_status = order_data['orderStatus']
        timestamp = order_data['updatedTime']

        # Map order status to the expected log entry type
        order_type = 'NEW' if order_status in ('New', 'PartiallyFilled') else 'CANCEL'

        # Construct log entry
        order_log_entry = f"{order_id}:{symbol}:{side}:{price}:{qty}:{order_type}:{timestamp}"

        # Add new entry to the order history
        self.order_record.append(order_log_entry)

        # Keep the last 20 order history entries
        self.order_record = self.order_record[-20:]

        print(len(self.order_record), self.order_record)

    def handle_execution_message(self, message):
        print("Execution Message: ", message)

    async def create_order(self, symbol, side, qty, price, order_type="Limit", timeInForce="PostOnly"):
        max_retry = 5
        for attempt in range(max_retry):
            try:
                order = self.session.place_order(
                    category="linear",
                    symbol=symbol,
                    side=side,
                    orderType=order_type,
                    qty=qty,
                    price=price,
                    timeInForce=timeInForce,
                    isLeverage=0,
                    orderFilter="Order",
                )
                return order
            except Exception as e:
                if attempt < max_retry - 1:
                    await asyncio.sleep(3)
                else:
                    raise e

    async def buy_order(self, rb):
        response = await self.create_order(self.symbol, 'Buy', self.size, str(rb))
        return response

    async def sell_order(self, ra):
        response = await self.create_order(self.symbol, 'Sell', self.size, str(ra))
        return response

    async def cancel_order(self, symbol, order_id):
        max_retry = 5
        for attempt in range(max_retry):
            try:
                response = self.session.cancel_order(
                    category="linear",
                    symbol=symbol,
                    orderId=order_id
                )
                return response
            except Exception as e:
                if attempt < max_retry - 1:
                    await asyncio.sleep(3)
                else:
                    raise e

    async def async_cancel_all_orders(self):
        max_retry = 5
        for attempt in range(max_retry):
            try:
                response = self.session.cancel_all_orders(
                    category="linear",
                    settleCoin="USDT",
                )
                return response
            except Exception as e:
                if attempt < max_retry - 1:
                    await asyncio.sleep(3)
                else:
                    raise e

    async def order_tasks(self, ra, rb):
        asyncio.create_task(self.buy_order(rb))
        asyncio.create_task(self.sell_order(ra))

    async def async_get_instruments_info(self):
        response = self.session.get_instruments_info(
            category="linear",
            symbol=self.symbol,
        )
        self.market_info = response
        self.price_scale = response['result']['list'][0]['priceScale']

    async def connect_and_subscribe(self, loop):
        while True:
            try:
                ws = WebSocket(
                    testnet=False,
                    api_key=self.api_key,
                    api_secret=self.api_secret,
                    channel_type="linear",
                )
                ws_private = WebSocket(
                    testnet=False,
                    channel_type="private",
                    api_key=self.api_key,
                    api_secret=self.api_secret,
                )
                ws.orderbook_stream(
                    depth=50,
                    symbol=self.symbol,
                    callback=lambda msg: self.handle_message(loop, msg)
                )
                ws.trade_stream(
                    symbol=self.symbol,
                    callback=lambda msg: self.handle_message(loop, msg)
                )
                ws.kline_stream(
                    interval=1,
                    symbol="BTCUSDT",
                    callback=self.handle_message
                )
                ws_private.order_stream(callback=self.handle_private_order_message)
                ws_private.execution_stream(callback=self.handle_execution_message)
                while True:
                    await asyncio.sleep(1)
            except Exception as e:
                print(f'Error occurred with WebSocket connection: {e}. Reconnecting...')
                await asyncio.sleep(5)  # wait before trying to reconnect

    async def main(self):
        loop = asyncio.get_event_loop()
        await self.async_get_instruments_info()
        await self.async_cancel_all_orders()
        await self.connect_and_subscribe(loop)


if __name__ == "__main__":
    market_maker = MarketMaker()
    asyncio.run(market_maker.main())
